// https://intense-basin-95528.herokuapp.com/

const
  bodyParser = require('body-parser'),
  express = require('express'),
  RSVP = require('rsvp'),
  config = require('config')

const
    StartAuthenticationAPI = require('./api/startAuthentication.js'),
    TOTPAPI = require('./api/totp.js')

const ATS_CALLBACK_URL = (process.env.ATS_CALLBACK_URL) ?
  process.env.ATS_CALLBACK_URL :
  config.get('atsCallbackURL');

const ATS_API_KEY = (process.env.ATS_API_KEY) ?
  process.env.ATS_API_KEY :
  config.get('atsAPIKey');

const MONGODB_URL = (process.env.MONGODB_URL) ?
  process.env.MONGODB_URL :
  config.get('mongodbURL');

const DB_TOTP_REQUESTS = (process.env.DB_TOTP_REQUESTS) ?
  process.env.DB_TOTP_REQUESTS :
  config.get('dbTotpRequests');

const PUSH_NOTIFICATION_URL = (process.env.PUSH_NOTIFICATION_URL) ?
  process.env.PUSH_NOTIFICATION_URL :
  config.get('pushNotificationURL');

const PUSH_NOTIFICATION_APPID = (process.env.PUSH_NOTIFICATION_APPID) ?
  process.env.PUSH_NOTIFICATION_APPID :
  config.get('pushNotificationAppID');

const
  app = express()

StartAuthenticationAPI.initialize(MONGODB_URL, DB_TOTP_REQUESTS, PUSH_NOTIFICATION_URL, PUSH_NOTIFICATION_APPID)
TOTPAPI.initialize(MONGODB_URL, DB_TOTP_REQUESTS, ATS_CALLBACK_URL, ATS_API_KEY)

// Express setup
app.set('port', process.env.PORT || 5000);
// app.use(xhub({ algorithm: 'sha1', secret: HMAC_SECRET }));
app.use(bodyParser.json({
  type: function() {
        return true;
    }
}));

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});

app.post('/startAuthentication', StartAuthenticationAPI.post)
app.post('/totp', TOTPAPI.post)
app.get('/totp', TOTPAPI.get)
app.delete('/totp', TOTPAPI.delete)
