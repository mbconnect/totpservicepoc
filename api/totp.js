const
  RSVP = require('rsvp'),
  request = require('request'),
  mongoClient = require('mongodb').MongoClient;

const
  TOTPAPI = {};

(function(){
  TOTPAPI.mongoDB_URL = ""
  TOTPAPI.mongoDB_Collection = ""

  TOTPAPI.atsCallbackURL = ""
  TOTPAPI.atsCallbackAPIKey = ""

  TOTPAPI.totpRequestCollection = {}

  // Init function
  TOTPAPI.initialize = initializeTOTPAPI

  // Implementation of the various routes
  TOTPAPI.post = doTOTP_POST
  TOTPAPI.get = doTOTP_GET
  TOTPAPI.delete = doTOTP_DELETE
})()

module.exports = TOTPAPI

function initializeTOTPAPI(mongoDB_URL, mongoDB_Collection, atsCallbackURL, atsCallbackAPIKey){
  console.log( 'initializeStartAuthenticationAPI()');
  console.log( '  mongoDB_URL=' + mongoDB_URL);
  console.log( '  mongoDB_Collection=' + mongoDB_Collection);

  TOTPAPI.mongoDB_URL = mongoDB_URL
  TOTPAPI.mongoDB_Collection = mongoDB_Collection

  TOTPAPI.atsCallbackURL = atsCallbackURL
  TOTPAPI.atsCallbackAPIKey = atsCallbackAPIKey

  mongoClient.connect(mongoDB_URL, function(err, db) {
    console.log( 'initializeStartAuthenticationAPI: mongoClient connected to: ' + mongoDB_URL);

    if(err){
      console.log(err);
      process.exit(-1);
    } else {
      db.createCollection(mongoDB_Collection, function(err, collection) {
        console.log( 'initializeStartAuthenticationAPI: collection created/loaded: ' + mongoDB_Collection);
        TOTPAPI.totpRequestCollection = collection;
      });
    }
  });
}

function doTOTP_DELETE(req, res){
  console.log('DELETE /totp?' + req.query.deviceToken);
  console.log( ' Headers: ' + JSON.stringify(req.headers));
  console.log( ' Parameters: ' + JSON.stringify(req.params));
  console.log( ' Query: ' + JSON.stringify(req.query));
  console.log( ' Body: ' + JSON.stringify(req.body));

  var deviceToken = req.query.deviceToken;
  updateStatus({deviceToken: deviceToken, status:"auth_cancel"}).then(
    function(){
      console.log( 'DELETE /totp: authentication cancelled, calling back ATS with auth_cancel');
      res.status(200).send({"status":100});
    }, function(error){
      res.status(200).send({"status":200});
    });
}

function doTOTP_GET(req, res){
  console.log('GET /totp?' + req.query.deviceToken);
  console.log( ' Headers: ' + JSON.stringify(req.headers));
  console.log( ' Parameters: ' + JSON.stringify(req.params));
  console.log( ' Query: ' + JSON.stringify(req.query));
  console.log( ' Body: ' + JSON.stringify(req.body));

  TOTPAPI.totpRequestCollection.findOne({_id: req.query.deviceToken, "status":"auth_init"}, function(err, item){
    if(err){
      console.log( 'error');
      res.status(200).send()
    } else {
      if(!item){
        console.log( 'item not found');
        res.status(200).send({"status":200});
      } else {
        console.log( 'item found, returning');
        console.log(JSON.stringify(item));
        res.status(200).send({
          "status":100,
          "name":"ingo.pansa@icconsult.com",
          "userAgent":"",
          "applicationName":""
        });
      }
    }
  })
}

function doTOTP_POST(req, res){
  console.log('POST /totp');
  console.log( ' Headers: ' + JSON.stringify(req.headers));
  console.log( ' Parameters: ' + JSON.stringify(req.params));
  console.log( ' Query: ' + JSON.stringify(req.query));
  console.log( ' Body: ' + JSON.stringify(req.body));

  var totp = req.body.totp;
  var deviceToken = req.body.deviceToken;

  verifyTOTP({totp:totp, deviceToken: deviceToken}).then(
    function(result){
      console.log( 'POST /totp: verification returned with='+result.status + ', calling back ATS with auth_success');
      // TODO: Handle verify_failed case
      updateStatus({deviceToken: deviceToken, status:"auth_success"}).then(
        function(){
          res.status(200).send({"status":100});
        }, function(error){
          res.status(200).send({"status":200});
        });
      // callbackActionTokenService(totpRequestID, "auth_success");
    }, function(error){
      console.log( 'POST /totp: verification NOT successfull, calling back ATS with auth_failed');
      // callbackActionTokenService(totpRequestID, "auth_failed");
      // res.status(200).send({"status":200});
      updateStatus({deviceToken: deviceToken, status:"auth_failed"}).then(
        function(){
          res.status(200).send({"status":200});
        }, function(error){
          res.status(200).send({"status":200});
        });
    });
}


function verifyTOTP(params){
  var totp = params.totp;
  var deviceToken = params.deviceToken;
  console.log( 'verifyTOTP(): totp=' + totp + ' deviceToken=' + deviceToken);

  return new RSVP.Promise(function(resolve, reject){
    // TODO: implement "real" verification and provide status=verify_failed
    resolve({status: "verify_success"});
  });
}

function updateStatus(params){
  var deviceToken = params.deviceToken;
  var status = params.status;

  console.log( 'updateStatus(): deviceToken=' + deviceToken + ' status=' + status);

  return new RSVP.Promise(function(resolve, reject){
    TOTPAPI.totpRequestCollection.findOne({_id: deviceToken},function(err, item){
      if(err){
        console.log( 'updateStatus(): error=' + error.message );
        reject(err.message);
      } else {
        if(!item){
          console.log( 'updateStatus(): item not found' );
          reject('item not found')
        } else {
          console.log( 'item=' + JSON.stringify(item));
          TOTPAPI.totpRequestCollection.update({_id:deviceToken},{$set:{status:status}})
          callbackActionTokenService(item.totpRequestID, status);
          resolve();
        }
      }
    });
  });
}

function callbackActionTokenService(totpRequestID, status){
  request.post({
    url: TOTPAPI.atsCallbackURL,
    headers: {
      "apikey":TOTPAPI.atsCallbackAPIKey,
      "Content-Type":"application/json",
      "Accept":"application/json"
    },
    json:{
      "totpRequestID": totpRequestID,
      "authenticationStatus": status
    }
  }, function(err, response, body){
    if(err){
      console.log(err);
      throw new Error('error starting authentication')
    } else {
      console.log( 'POST /startAuthentication: successfully authenticated');
    }
  })
}
