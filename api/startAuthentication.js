const
  RSVP = require('rsvp'),
  request = require('request'),
  mongoClient = require('mongodb').MongoClient;

const
  StartAuthenticationAPI = {};

(function(){
  StartAuthenticationAPI.pushNotificationURL = ""
  StartAuthenticationAPI.pushNotificationAppID = ""

  StartAuthenticationAPI.mongoDB_URL = ""
  StartAuthenticationAPI.mongoDB_Collection = ""
  StartAuthenticationAPI.totpRequestCollection = {}

  // Init function
  StartAuthenticationAPI.initialize = initializeStartAuthenticationAPI

  // Implementation of the various routes
  StartAuthenticationAPI.post = doStartAuthentication_POST
})()

module.exports = StartAuthenticationAPI

function initializeStartAuthenticationAPI(mongoDB_URL, mongoDB_Collection, pushNotificationURL, pushNotificationAppID){
  console.log( 'initializeStartAuthenticationAPI()');
  console.log( '  mongoDB_URL=' + mongoDB_URL);
  console.log( '  mongoDB_Collection=' + mongoDB_Collection);

  StartAuthenticationAPI.mongoDB_URL = mongoDB_URL
  StartAuthenticationAPI.mongoDB_Collection = mongoDB_Collection
  StartAuthenticationAPI.pushNotificationURL = pushNotificationURL
  StartAuthenticationAPI.pushNotificationAppID = pushNotificationAppID


  mongoClient.connect(mongoDB_URL, function(err, db) {
    console.log( 'initializeStartAuthenticationAPI: mongoClient connected to: ' + mongoDB_URL);

    if(err){
      console.log(err);
      process.exit(-1);
    } else {
      db.createCollection(mongoDB_Collection, function(err, collection) {
        console.log( 'initializeStartAuthenticationAPI: collection created/loaded: ' + mongoDB_Collection);
        StartAuthenticationAPI.totpRequestCollection = collection;
      });
    }
  });
}

function doStartAuthentication_POST(req, res){
  console.log('doStartAuthentication_POST()');
  console.log( ' Headers: ' + JSON.stringify(req.headers));
  console.log( ' Parameters: ' + JSON.stringify(req.params));
  console.log( ' Body: ' + JSON.stringify(req.body));

  var totpRequestID =req.body.totpRequestID;
  var audience = req.body.aud;
  var subject = req.body.sub;

  // first, retrieve the userProfileInformation
  // we need to obtain the following information:
  //  - deviceToken: this is the deviceID of the registered push notification device
  //  - pushNotificationRecipientID: this is userID of the registered push notification recipient
  retrieveUserProfileInformation({userID: req.body.userID}).then(
    function(userProfile){
      var deviceToken = userProfile.deviceToken;
      var pushNotificationRecipientID = userProfile.pushNotificationRecipientID;

      // create a record in the TOTP Request collection
      // the deviceToken will be used by authenticator applications to retrieve any pending authentication requests
      // in fact, we do an upsert, which means if the deviceToken does not yet exist, it will be created, otherwise any pending request will be overwritten
      // TODO: add timestamp, implement purge function for outdated requests
      StartAuthenticationAPI.totpRequestCollection.update(
        {
          _id:deviceToken
        },
        {
          _id:deviceToken,
          totpRequestID: totpRequestID,
          subject: subject,
          pushNotificationRecipientID: pushNotificationRecipientID,
          audience: audience,
          status:"auth_init"
        },
        {
          upsert:true
        });

      // Then, send a push notification to the registered device
      sendPushNotification(userProfile, audience, "Lights, Location, Doors").then(
        function(result){
          res.status(200).send('push notification sent');
        }, function(error){
          res.status(400).send('could not send push notification');
        });
    }, function(error){
      res.status(400).send('could not retrieve user profile information');
    });
}

function sendPushNotification(userProfile, audience, features){
  console.log( 'sendPushNotification()' );
  console.log( '  userProfile=' + JSON.stringify(userProfile) );
  console.log( '  audience=' + audience );
  console.log( '  features=' + features );
  console.log( '  url=' + StartAuthenticationAPI.pushNotificationURL );
  console.log( '  app_id=' + StartAuthenticationAPI.pushNotificationAppID );


  return new RSVP.Promise(function(resolve, reject){
    request.post({
      url: StartAuthenticationAPI.pushNotificationURL,
      headers: {
        "Content-Type":"application/json",
        "Accept":"application/json"
      },
      json:{
        "app_id": StartAuthenticationAPI.pushNotificationAppID,
        "include_player_ids": [userProfile.pushNotificationRecipientID],
        "data": {"foo": "bar"},
        "headings": {"en":"Approve Access"},
        "contents": {"en": audience + " wants access to the following car feature(s): "+features}
      }
    }, function(err, response, body){
      if(err){
        console.log(err);
        reject('error sending push notification')
      } else {
        console.log( 'successfully sent push notification');
        resolve();
      }
    })
  })
}

function retrieveUserProfileInformation(params){
  console.log( 'retrieveUserProfileInformation()' );
  console.log( '  params=' + JSON.stringify(params) );

  return new RSVP.Promise(function(resolve, reject){
    var userProfile = {
      userID: "ciam1234",
      deviceToken: "ab4ff219bde151f15ad94d7e10b0d3bb038dc194c0b8ecd357473b4f8b90bbca",
      pushNotificationRecipientID: "5c307aee-bc40-477b-96da-4a8ec7a34d0b"
    }

    console.log( 'retrieveUserProfileInformation(): userProfile=' + JSON.stringify(userProfile) );
    resolve(userProfile);
  });
}
